rmdups.sh
============

This *unmaintanable* bash script aims to help in the difficult job to choose the right duplicated files to remove.

##### Other alternatives (more mature and faster)
- [Duplicate file finders on Wikipedia](https://en.wikipedia.org/wiki/List_of_duplicate_file_finders)
- [fslint](http://www.pixelbeat.org/fslint/)
- `duff`
- [`fdups`](http://en.wikipedia.org/wiki/Fdupes)
- [`rmlint`](https://github.com/sahib/rmlint)
- ... (proposes yours here)

##### Also handling similar image content
- [`findimagedups` from Jonathan H N Chin](http://www.jhnc.org/findimagedupes/), perl script (and C lib) storing image fingerprints into a Berkley DB file and printing together filenames of images matching more than xx% similarity (pictures taken in burst mode may be flagged as similar)
- [`findimagedupes` version in Go](https://github.com/opennota/findimagedupes)
- [gThumb](https://en.wikipedia.org/wiki/GThumb) can also [find/remove duplicates](http://www.webupd8.org/2011/03/gthumb-2131-released-with-find.html)
- [Geeqie](https://en.wikipedia.org/wiki/Geeqie)
- [imgSeek](http://www.imgseek.net/)
- [digiKam](https://en.wikipedia.org/wiki/DigiKam) and its [Find Duplicate Images Tool](http://www.digikam.org/node/333)
- [Visipics](www.visipics.info)
- [dupeGuru Picture Edition](http://www.hardcoded.net/dupeguru_pe/)
